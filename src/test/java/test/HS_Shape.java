package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class HS_Shape extends BaseClass{
	@Test(priority=1, enabled=true)
	public void Shape() {
		test = extent.createTest("ClICK ON SHAPE BUTTON","This test case will click on shape button");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"hui main shape\"]")).click();
		test.log(Status.PASS, "Shape button is  clicked");
	}
	@Test(priority=2, enabled=true)
	public void EnterBMI() throws InterruptedException {
		test = extent.createTest("ADD SHAPE DATA","This test case will add shape data");
		test.log(Status.INFO, "Started");
		MobileElement e1= driver.findElement(By.xpath(
				"//XCUIElementTypeButton[@name=\"Button\"]"));
		ActionMethods.tapByElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked");
		MobileElement e2= driver.findElement(By.xpath( 
				"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[1]/XCUIElementTypePickerWheel"));
		ActionMethods.scrollOneStepUP(e2);
		test.log(Status.PASS, "weight list is scrolled");
		Thread.sleep(2000);
		MobileElement e3= driver.findElement(By.xpath(
				"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[2]/XCUIElementTypePickerWheel"));
		ActionMethods.scrollOneStepsDown(e3);
		test.log(Status.PASS, "weight list is scrolled");

		driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton")).click();	
	    test.log(Status.PASS, "BMI entered successfully");
	}
	@Test(priority=3, enabled=true)
	public void ClickBMIDeleteBtton() throws InterruptedException {
		test = extent.createTest("DELETE SLEEP SCORE","This test case will delete sleep score individually");
		test.log(Status.INFO, "Started");
	   MobileElement deleteicon=driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]"));
	   ActionMethods.WaitByvisibilityOfElement(deleteicon, 2);
	   ActionMethods.tapByElement(deleteicon);
	   test.log(Status.PASS,"Delete Button Click on BMI");
	}
	@Test(priority=4, enabled=true)
	public void DeleteBMI() throws InterruptedException {
		  MobileElement deleteBMI= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton[2]"));
		  ActionMethods.tapByElement(deleteBMI); 
		  driver.switchTo().alert().dismiss();
			test.log(Status.PASS, "BMI score is deleted");	
			Thread.sleep(3000);
		}
	@Test(priority=5, enabled=true)
	public void resetBMI() throws InterruptedException {
		
		test = extent.createTest("RESET MIND SCORE","This test case will reset mind score");
		test.log(Status.INFO, "Test Case Started");
		EnterBMI();
		ClickBMIDeleteBtton();
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"RESET ALL\"]")).click();
		driver.switchTo().alert().dismiss();
		//XCUIElementTypeButton[@name="CANCEL"]
		test.log(Status.PASS, "Reset All button is clicked ");	
		Thread.sleep(3000);
		test.log(Status.PASS, "BMI is score deleted ");	
	} 
	@Test(priority=6, enabled=true)
	public void VerifyCancleButtonBMI() throws InterruptedException {

		test = extent.createTest("CANCEL DELETE ACTION","This test case will cancle delete Action On BMI");
		test.log(Status.INFO, "Test Case Started");
		ClickBMIDeleteBtton();
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CANCEL\"]")).click();
		test.log(Status.PASS, "Cancel button is clicked ");	
		Thread.sleep(3000);
		test.log(Status.PASS, "BMI is score not deleted ");	
	}
	@Test(priority=7, enabled=true)
	public void BacktoHealthScore() {
		test = extent.createTest("NAVIGATE BACK TO HEALTHSCORE MODULES","This test case will navigate back by clicking back button");
		MobileElement backButton= driver.findElement(By.xpath(
				"//XCUIElementTypeNavigationBar[@name=\"SHAPE\"]/XCUIElementTypeButton[1]"));
		ActionMethods.tapByElement(backButton);	
		test.log(Status.PASS, "Back button clicked");
	}
	
}
