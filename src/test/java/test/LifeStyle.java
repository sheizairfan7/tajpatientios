package test;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class LifeStyle  extends BaseClass{
	public ExtentTest test;
	@Test(priority=1,enabled=true)
	public void clickOnMenuIcon(){
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	}
	@Test(priority=2,enabled=true)
	public void tapOnMedicalprofile(){

		test =extent.createTest("CLICK MEDICAL PROFILE","This Test Case will click on medical profile.");
		test.log(Status.INFO,"Test case Started");

		driver.findElement(MobileBy.AccessibilityId("medical profile")).click();

		test.log(Status.PASS, "Medical profile clicked");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Test(priority=3,enabled=false)
	public void lifeStyle(){
		test =extent.createTest("ADD LIFESTYLE DATA","This test case will add lifestyle data of patient");
		test.log(Status.INFO,"Test case Started");
		MobileElement tab2 =driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[2]"));
		ActionMethods.WaitByvisibilityOfElement(tab2,2000);
		ActionMethods.tapByElement(tab2);
		
		//XCUIElementTypeStaticText[@name="Do you smoke/use tobbaco?"]

		//XCUIElementTypeStaticText[@name="Have you traveled overseas in the past 2 months?"]
		test.log(Status.PASS, "clicked on Life style tab");
	}

	@Test(priority=4,enabled=false)
	public void lifeStyleOptions() throws InterruptedException{
		test =extent.createTest("CLICK ON LIFESTYLE SWITCH","This test case will on/off switch");
		test.log(Status.INFO,"Test case Started");
		driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name=\"Do you smoke/use tobbaco?\"]")).click();
		Thread.sleep(2000);
		test.log(Status.PASS, "Clicked on 1st Switch, if the toggle is ON it will turn if OFF and Vice versa");
		driver.findElement(By.xpath("//XCUIElementTypeSwitch[@name=\"Have you traveled overseas in the past 2 months?\"]")).click();
		test.log(Status.PASS, "Clicked on 2nd Switch, if the toggle is ON it will turn if OFF and Vice versa");
		Thread.sleep(2000);
	}
	//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel
	//XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]

	//HEALTH PROBLEM
		@Test(priority=5,enabled= false)
		public void healthProblems(){	
			test =extent.createTest("ADD HEALTH PROBLEMS DATA","This test case will Enter Health problems of patient");
			test.log(Status.INFO,"Test case Started");
			driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[3]")).click();
			test.log(Status.PASS, "clicked Health Problem Tab");
		}
//		try {
//			driver.executeScript("mobile: selectPickerWheelValue", param);		
//		} catch(Exception e) {
//			System.out.println("Ignoring element exception, moving back one step");
//			param.put("order", "previous");
//			try {
//				driver.executeScript("mobile: selectPickerWheelValue", param);
//
//			}catch()
//			
	//	}
public void saveSeverity() {
	MobileElement setSeverity = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel"));
	ActionMethods.scrollTwoStepUp(setSeverity);
	test.log(Status.PASS, "Severity of problem is set");
	MobileElement savebtn =driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Save\"]"));
	ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
	ActionMethods.tapByElement(savebtn);
	test.log(Status.PASS, "Save button clicked");
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
		@Test(priority=6, enabled=false)
		public void healthProblemList(){	
			MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
			ActionMethods.tapOnElement(problem1);
			test.log(Status.PASS, "First problem is selected from list @index=1");
			saveSeverity();	
			healthProblems();
			MobileElement problem3 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]"));
		    ActionMethods.tapOnElement(problem3);
			test.log(Status.PASS, "Third problem is selected from list @index=3");
			saveSeverity();
			healthProblems();
			MobileElement problem6 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]"));
			ActionMethods.tapOnElement(problem6);		   
			test.log(Status.PASS, "Sixth problem is selected from list @index=6");
			saveSeverity();
			healthProblems();
		}

		@Test(priority=7, enabled=false)
		public void allergiesTab(){	
			test =extent.createTest("ADD ALLERGIES DATA","This test case will Enter Allergies data for patient");
			test.log(Status.INFO,"Test case Started");
			driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[5]")).click();
			test.log(Status.PASS, "Clicked on Allergies Tab");
		}
		@Test (priority=8, enabled=false)
			public void allergiesList() {
			MobileElement problem1 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
	        ActionMethods.tapOnElement(problem1);
			test.log(Status.PASS, "First problem is selected from list @index=1");
			saveSeverity();	
			
			MobileElement problem3 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]"));
		    ActionMethods.tapOnElement(problem3);
			test.log(Status.PASS, "Third problem is selected from list @index=3");
			saveSeverity();
			
			MobileElement problem6 =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[6]"));
			ActionMethods.tapOnElement(problem6);		   
			test.log(Status.PASS, "Sixth problem is selected from list @index=6");
			saveSeverity();		
			}
		@Test(priority=19,enabled=true)
		public void ongoingTreatment (){
			test =extent.createTest("ADD ONGOING TREATMENT DATA","This test case will treatment data.");
			test.log(Status.INFO,"Test case Started");
			driver.findElement(By.xpath("//XCUIElementTypeTabBar[@name=\"Tab Bar\"]/XCUIElementTypeButton[4]")).click();
			test.log(Status.PASS, "clicked on  Ongoing Treatment tab");
		}
		@Test(priority=20,enabled=true)
		public void addOngoingTreatment() throws InterruptedException{
			//driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"add new\"]")).click();
			driver.findElement(MobileBy.AccessibilityId("add new")).click();
			test.log(Status.PASS, "clicked on  Add Button");
			//XCUIElementTypeStaticText[@name="No Medical History"]
			String medicineTextField="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
			MobileElement medicineName=driver.findElement(By.xpath(medicineTextField));
			medicineName.sendKeys("panadol");
			test.log(Status.PASS, "Panadol is entered, Matching record returned");
			Thread.sleep(3000);
			String medicineListxpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeTable/XCUIElementTypeCell[1]";
		    MobileElement medicine= driver.findElement(By.xpath(medicineListxpath));
			ActionMethods.WaitByvisibilityOfElement(medicine, 3000);
			//ActionMethods.tapOnElement(medicine);
			medicine.click();
			test.log(Status.PASS, "First item is selected from list of record");
			test.log(Status.PASS, "Medicine Name added ");
            String MedicineDatexpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
            MobileElement medicineDate=driver.findElement(By.xpath(MedicineDatexpath));
            ActionMethods.tapByElement(medicineDate);
		    medicineDate.clear();
			//medicineDate.sendKeys("20-05-2020");
			//MobileElement date =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther"));
     		//ActionMethods.WaitByvisibilityOfElement(date, 5000);
		   // ActionMethods.tapByElement(date);
			//date.click();
		//	ActionMethods.tabByCoordinates(5, 703);
		    MobileElement savebtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"));
			ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
			ActionMethods.tapByElement(savebtn);

			// tap an element very near its top left corner
//			Map<String, Object> args1 = new HashMap();
//			args.put("element", ((MobileElement)date ).getId());
//			args.put("x", 5);
//			args.put("y", 702);
//			driver.executeScript("mobile: tap", args1);
		//	Thread.sleep(2000);
		//	ActionMethods.tapByCoordinates(5, 703);
			//driver.switchTo().alert();
			//date.click();
			test.log(Status.PASS, "Entered Last visit date");

//		MobileElement datepickerContainer=driver.findElement(By.xpath("//XCUIElementTypeOther[@name=\"Preview\"]/XCUIElementTypeOther/XCUIElementTypeDatePicker/XCUIElementTypeOther/XCUIElementTypeOther"));
//			//XCUIElementTypeButton[@name="Month"]
//		ActionMethods.WaitByvisibilityOfElement(datepickerContainer, 8000);
//			ActionMethods.tapByElement(datepickerContainer);
			//XCUIElementTypeButton[@name="Previous Month"]
			
			//XCUIElementTypeButton[@name="Next Month"]
			
			//XCUIElementTypeOther[@name="Preview"]/XCUIElementTypeOther/XCUIElementTypeDatePicker/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeDatePicker/XCUIElementTypePicker/XCUIElementTypePickerWheel[1]
			
			//XCUIElementTypeOther[@name="Preview"]/XCUIElementTypeOther/XCUIElementTypeDatePicker/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeDatePicker/XCUIElementTypePicker/XCUIElementTypePickerWheel[2]
			
			//XCUIElementTypeButton[@name="Hide year picker"]
			
			//XCUIElementTypeStaticText[@name="2"]
			
			//test.log(Status.PASS, "Clicked on Date"); 
			//MobileElement savebtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"));
			//ActionMethods.WaitByvisibilityOfElement(savebtn, 5000);
			//ActionMethods.tapByElement(savebtn);

			//driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Save\"]")).click();
			//test.log(Status.PASS, "Clicked on Save button");
			Thread.sleep(5000);
		}

		@Test(priority=21,enabled=false)
		public void verifyCancelButtonAddTreatment(){

			test =extent.createTest("Verify Cancel Button on Adding Ongoing Treatment.");
			test.log(Status.INFO,"Test case Started");
			driver.findElement(By.id("ongoing_treatment_tab")).click();
			test.log(Status.INFO, "clicked on  Ongoing Treatment tab");
			driver.findElement(By.xpath("//android.widget.TextView[@text='Add New']")).click();
			test.log(Status.INFO, "clicked on  Add Button");
			driver.findElement(By.id("ll_cancel")).click();
			test.log(Status.INFO, "Clicked on Cancel button");
		}
		
		@Test(priority=23,enabled=false)
		void clickOnBack() throws InterruptedException {
			Thread.sleep(3000);
			driver.findElement(By.id("iv_menu_back")).click();
			
		}
		
			
		}
