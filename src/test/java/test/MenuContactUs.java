package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class MenuContactUs extends BaseClass{

	@Test(priority=1,enabled=true)
	public void clickOnMenuIcon(){
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	
		driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	}
	@Test(priority=2 , enabled=true)
	public void contactus() throws InterruptedException
	{
		test = extent.createTest("SUBMIT CONTACT US FORM","This test case will submit contact us form");
		test.log(Status.INFO, "Test case Started");
		MobileElement contactUs= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Contact Us\"]"));
		//MobileElement contactUs= driver.findElement(MobileBy.AccessibilityId("Contact Us"));
		ActionMethods.WaitByvisibilityOfElement(contactUs, 5000);
		ActionMethods.tapByElement(contactUs);
		test.log(Status.PASS, "Contactus button clicked");
	}
	@Test(priority=3, enabled=true)
	public void SendComplaint() throws InterruptedException {
		test.log(Status.INFO, "Select issue");
		
		String xpathIssuetype ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
		MobileElement issuetype= driver.findElement(By.xpath(xpathIssuetype));
		ActionMethods.WaitByvisibilityOfElement(issuetype, 5000);
		ActionMethods.tapByElement(issuetype);
        String xpathIssuePicker="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypePicker/XCUIElementTypePickerWheel";
        MobileElement issuepicker= driver.findElement(By.xpath(xpathIssuePicker));
		ActionMethods.WaitByvisibilityOfElement(issuepicker, 5000);
		ActionMethods.scrollOneStepsDown(issuepicker);
		//ActionMethods.tapByElement(issuepicker);
		//XCUIElementTypeStaticText[@name="Issue"]
		test.log(Status.PASS, "Issue Selected from list");
		
		String toolbar="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]";
		MobileElement toolbars= driver.findElement(By.xpath(xpathIssuePicker));
		ActionMethods.WaitByvisibilityOfElement(toolbars, 5000);
		ActionMethods.tapByElement(toolbars);
		ActionMethods.tapByCoordinates(400, 485);
		test.log(Status.PASS, "Click Done button");
        String xpathAddComment ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView";
        MobileElement addComment= driver.findElement(By.xpath(xpathAddComment));
 		ActionMethods.WaitByvisibilityOfElement(addComment, 5000);
 		addComment.sendKeys("comment text");
 		MobileElement hideKB= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Add comments\"]"));
        ActionMethods.WaitByvisibilityOfElement(hideKB, 5000);
        ActionMethods.tapByElement(hideKB);
 		test.log(Status.PASS, "Comments entered");
        String xpathAddEmail ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
        MobileElement addEmail= driver.findElement(By.xpath(xpathAddEmail));
		ActionMethods.WaitByvisibilityOfElement(addEmail, 5000);
		addEmail.sendKeys("email@gmail.com");
		MobileElement hidekboard= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Email Address\"]"));
        ActionMethods.WaitByvisibilityOfElement(hidekboard, 5000);
        ActionMethods.tapByElement(hidekboard);
		test.log(Status.PASS, "email address entered");
		MobileElement sendComplaint= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"SEND\"]"));
		ActionMethods.WaitByvisibilityOfElement(sendComplaint, 5000);
        //ActionMethods.tapByElement(sendComplaint);
		sendComplaint.click();
		test.log(Status.PASS, "send button clicked");
		Thread.sleep(5000);
}
}
