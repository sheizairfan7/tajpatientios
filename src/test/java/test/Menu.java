package test;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class Menu extends BaseClass{

	
	ExtentTest test;	
//	@Test(priority=1 ,enabled=true)
//	public void testFAQs() throws InterruptedException 
//	{
//		test = extent.createTest("VIEW FAQS","This test case will open faqs");
//		test.log(Status.INFO, "Test case Started");
//		wait = new WebDriverWait(driver,10);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='FAQs']")));
//		MobileElement e1 =driver.findElement(By.xpath("//android.widget.TextView[@text='FAQs']"));
//		ActionMethods.tapOnElement(e1);
//		test.log(Status.PASS, "FAQ's button clicked");
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//android.widget.TextView[@text='Who are your practitioners?']")));
//		MobileElement e2 =driver.findElement(By.xpath("//android.widget.TextView[@text='Who are your practitioners?']"));
//		ActionMethods.tapOnElement(e2);
//		test.log(Status.PASS, "first question opened");
//		Thread.sleep(3000);
//		driver.findElement(By.id("iv_menu_back")).click();
//	}
 
	@Test(priority=1,enabled=true)
	public void clickOnMenuIcon(){
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	}
	
	@Test(priority=2 , enabled=true)
	public void Consultation() throws InterruptedException
	{
		test = extent.createTest("CLICK ON CONSULTATION","This test case will click on consultation from menu");
		test.log(Status.INFO, "Test case Started");
		MobileElement consultation= driver.findElement(MobileBy.AccessibilityId("consultation"));
		ActionMethods.WaitByvisibilityOfElement(consultation, 5000);
		ActionMethods.tapByElement(consultation);
		test.log(Status.PASS, "Clicked on consultation");
	}
	@Test(priority=3,enabled= true)
	public void noConsultation() throws InterruptedException 
	{   boolean bool = true;
		test = extent.createTest("CHECK CONSULTATION RECORD","This test case will check if consultation history present or not");
		test.log(Status.INFO, "Test case Started");
		SoftAssert assertion=new SoftAssert();
		//verify consultation record

		//naviagtionBar xpath
		//XCUIElementTypeNavigationBar[@name="Consultations"]/XCUIElementTypeButton[1]
		//XCUIElementTypeStaticText[@name="Consultations"]
		//XCUIElementTypeNavigationBar[@name="Consultations"]/XCUIElementTypeButton[2]
		try {
		MobileElement Norecord=driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"We are glad you have not fallen sick yet\"]"));
		bool=Norecord.isDisplayed();
		assertion.assertFalse(Norecord.isDisplayed());
		System.out.println(Norecord.isDisplayed());
		//assertion.assertAll();
		test.log(Status.PASS, "No consultation history present");

//			if(bool==true) 
//			{
//			throw new SkipException("Skipping this exception");
//		}
		}
		catch(Exception e) {
			CosultationElement();
			//ConsultationListScroll();
			//throw new SkipException("Skipping this exception");
			}
		}
	@Test(priority=4, enabled=true,dependsOnMethods="noConsultation")
	public void CosultationElement() {
		
		SoftAssert assertion= new SoftAssert();
		try {
			MobileElement element= driver.findElement(By.xpath(
					"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]"));
			element.isDisplayed();
		assertion.assertTrue(element.isDisplayed());
		} catch(Exception e) {
			throw new SkipException("Skip testcase if element not present");
			
		}
	}
	@Test(priority=5,enabled=true, dependsOnMethods="CosultationElement")
	public void ConsultationListScroll() throws InterruptedException {
		test = extent.createTest("SCROLL CONSULTATION LIST","This test case will Scroll consultation list");
		test.log(Status.INFO, "Test case Started");
		String tableView="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable";
		RemoteWebElement element = (RemoteWebElement)driver. findElement(By.xpath(tableView));		
        ActionMethods.scrollDownByExecuteMobile(element);
		Thread.sleep(2000);
		test.log(Status.PASS, "Scrolled down on consultation");
		RemoteWebElement element1 = (RemoteWebElement)driver.findElement(By.className("XCUIElementTypeTable"));
		ActionMethods.scrollUpByExecuteMobile(element1);
		Thread.sleep(2000);
		test.log(Status.PASS, "Scrolled up on consultation");
	}
	@Test(priority=6, enabled=true, dependsOnMethods="ConsultationListScroll")
   public void 	ViewPrescription() throws InterruptedException{
		test = extent.createTest("View Prescription in consultation","This test case will open consultation prescription");
		test.log(Status.INFO, "Test case Started");
        String ConsultationCellxpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]";
	    MobileElement consultation=driver.findElement(By.xpath(ConsultationCellxpath));
	    ActionMethods.tapByElement(consultation);
	 	Thread.sleep(2000);
	 	RemoteWebElement scroll= driver.findElement(By.className("XCUIElementTypeTable"));
	 	ActionMethods.scrollDownByExecuteMobile(scroll);
	 	test.log(Status.PASS, "Scroll down prescription");
	 	
	 	//MobileElement medicine= driver.findElement(MobileBy.AccessibilityId("MEDICINE"));
	 	
	 	MobileElement medicine= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"MEDICINE\"]"));
	 	ActionMethods.WaitByvisibilityOfElement(medicine, 5);
	 	boolean bol= medicine.isDisplayed();
		System.out.println(bol);
		//ActionMethods.tapByElement(medicine);
	//	if(bol== true)
	//	{
//    String xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[11]/XCUIElementTypeOther[1]";	
//	MobileElement e= driver.findElement(By.xpath(xpath));
//	ActionMethods.tapByElement(e);
//	Thread.sleep(2000);
//	
//String back="//XCUIElementTypeButton[@name=\"my back\"]";
//MobileElement backbtn=driver.findElement(By.xpath(back));
//ActionMethods.tapByElement(backbtn);

String backtolist="//XCUIElementTypeNavigationBar[@name=\"Prescription\"]/XCUIElementTypeButton[1]";
MobileElement backbtnlist=driver.findElement(By.xpath(backtolist));
ActionMethods.tapByElement(backbtnlist);

//String home="//XCUIElementTypeNavigationBar[@name=\"Prescription\"]/XCUIElementTypeButton[2]";
//MobileElement homebtn=driver.findElement(By.xpath(home));
//ActionMethods.tapByElement(homebtn);

}
	//}
	@Test(priority=7, enabled=true, dependsOnMethods="ConsultationListScroll")
	public void rateDoctor() throws InterruptedException {
		
		//XCUIElementTypeButton[@name="ic rating cross"]
	//Apply assertion to check rating dialogue appears or not
		
		//XCUIElementTypeStaticText[@name="How was your consultation experience with doctor?"]
		
		//String ratingxpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther";
		//MobileElement element= driver.findElement(By.xpath(ratingxpath));
		//ActionMethods.WaitByvisibilityOfElement(element, 2);
		//XCUIElementTypeButton[@name="ic rating star"][3]
        MobileElement element1=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"ic rating star\"][3]"));
        ActionMethods.WaitByvisibilityOfElement(element1,5);
        ActionMethods.tapByElement(element1);
    	//Enter comments 
     String commentsXpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextView";
     MobileElement coments=driver.findElement(By.xpath(commentsXpath));
     ActionMethods.WaitByvisibilityOfElement(coments,5);
     coments.sendKeys("Had a good experience Talking to doctor");   
     //Enable Switch in rating dialouge
     //XCUIElementTypeApplication[@name="TAJ"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch
        MobileElement switches=driver.findElement(By.className("XCUIElementTypeSwitch"));
        ActionMethods.WaitByvisibilityOfElement(switches,5);
        ActionMethods.tapByElement(switches);
        Thread.sleep(3000);

        //Click rate doctor button
//        MobileElement rateDoc=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"RATE DOCTOR\"] "));
//        ActionMethods.WaitByvisibilityOfElement(rateDoc,5);
//        ActionMethods.tapByElement(rateDoc);
     
      //Click cross button  
        MobileElement cancleDialouge=driver.findElement(MobileBy.AccessibilityId("ic rating cross"));
      //  MobileElement cancleDialouge=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"ic rating cross\"]"));
        ActionMethods.WaitByvisibilityOfElement(cancleDialouge,5);
        ActionMethods.tapByElement(cancleDialouge);
      String home="//XCUIElementTypeNavigationBar[@name=\"Prescription\"]/XCUIElementTypeButton[2]";
      MobileElement homebtn=driver.findElement(By.xpath(home));
      ActionMethods.tapByElement(homebtn);
        Thread.sleep(5000);
	}
@Test(priority=8, enabled=false)
	public void Findpharmacy() {
	
	}
}