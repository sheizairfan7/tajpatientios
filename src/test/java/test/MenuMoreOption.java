package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class MenuMoreOption extends BaseClass {
	
@Test(priority=1, enabled=true)
public void MoreOption() {
	driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
	//test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	//XCUIElementTypeImage[@name="menu_more-2"]
	MobileElement moreOption=  driver.findElement(MobileBy.AccessibilityId("menu_more-2"));
	ActionMethods.tapByElement(moreOption);
}
@Test(priority=2, enabled=true)
public void Notification() throws InterruptedException {
	
	//XCUIElementTypeStaticText[@name="Notifications"]
	MobileElement notifications=  driver.findElement(MobileBy.AccessibilityId("Notifications"));
	ActionMethods.tapByElement(notifications);
	//XCUIElementTypeNavigationBar[@name="Notifications"]/XCUIElementTypeButton[1]
	Thread.sleep(2000);
	MobileElement backbtn=  driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"Notifications\"]/XCUIElementTypeButton[1]"));
	ActionMethods.tapByElement(backbtn);
	
}
@Test(priority=3, enabled=false)
public void TajGuide() throws InterruptedException {
	
	
	//XCUIElementTypeStaticText[@name="TAJ Guide"]
	MobileElement guide=  driver.findElement(MobileBy.AccessibilityId("TAJ Guide"));
	ActionMethods.WaitByvisibilityOfElement(guide, 5);
	ActionMethods.tapByElement(guide);
	//XCUIElementTypeNavigationBar[@name="Notifications"]/XCUIElementTypeButton[1]
	Thread.sleep(5000);
	ActionMethods.tapByCoordinates(50, 100);
	MobileElement canclevedio= driver.findElement(By.xpath(
			"//XCUIElementTypeNavigationBar[@name=\"AER_QA.AERGuideView\"]/XCUIElementTypeButton[1]"));
    ActionMethods.WaitByvisibilityOfElement(canclevedio, 5);
    ActionMethods.tapByElement(canclevedio);
    Thread.sleep(2000);
	MobileElement backbtn=  driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"Notifications\"]/XCUIElementTypeButton[1]"));
	ActionMethods.tapByElement(backbtn);
}
@Test(priority=4, enabled=true)
public void Share() throws InterruptedException {
	
	
	//XCUIElementTypeStaticText[@name="Share App"]
	MobileElement share=  driver.findElement(MobileBy.AccessibilityId("Share App"));
	ActionMethods.WaitByvisibilityOfElement(share, 5);
	ActionMethods.tapByElement(share);
	//XCUIElementTypeNavigationBar[@name="Notifications"]/XCUIElementTypeButton[1]
	Thread.sleep(5000);
	// Close share option
	MobileElement Closebtn=  driver.findElement(By.xpath( 
			"//XCUIElementTypeButton[@name=\"Close\"]"));
	ActionMethods.tapByElement(Closebtn);
}
@Test(priority=5, enabled=true)
public void backToMenu() {
	MobileElement backbtn= driver.findElement(By.xpath(
			"//XCUIElementTypeNavigationBar[@name=\"More\"]/XCUIElementTypeButton[1]"));
	ActionMethods.WaitByvisibilityOfElement(backbtn, 5);
	ActionMethods.tapByElement(backbtn);
}
}
