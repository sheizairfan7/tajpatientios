package test;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class HS_MindModule extends BaseClass{

@Test(priority=1, enabled=true)
public void HomeScreen_HealthScore() throws InterruptedException {
	test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
	test.log(Status.INFO, "Test Case Started");
	MobileElement element= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]"));
    //ActionMethods.tapByElement(element);
	ActionMethods.WaitByvisibilityOfElement(element, 5);
	element.click();
	test.log(Status.PASS, "Heathscore button is clicked");
Thread.sleep(4000);
}
//Topmenu xpath
//XCUIElementTypeNavigationBar[@name="LIFE STYLE SCORE"]/XCUIElementTypeButton[1]

//XCUIElementTypeStaticText[@name="LIFE STYLE SCORE"]
//Test Scenario 3: Verify that user can add HealthScore_Mind data
@Test(priority=2,enabled=true)
void lifestylescoremind() throws InterruptedException
{
	test = extent.createTest("ClICK ON MIND","This test case will click on mind button");
	test.log(Status.INFO, "Test Case Started");
	driver.findElement(By.xpath( "//XCUIElementTypeButton[@name=\"hui main mind\"]")).click();
	test.log(Status.PASS, " Mind button is clicked");
}

@Test(priority=3 , enabled=true)
void addsleepdata() throws InterruptedException
{
	test = extent.createTest("ADD SLEEP DATA","This test case will add sleep data");
	test.log(Status.INFO, "Test Case Started");
	MobileElement e1= driver.findElement(By.xpath(
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeButton[1]"));
	ActionMethods.tapOnElement(e1);
	test.log(Status.PASS, "Add "+" button is clicked ");
//	MobileElement seek_bar=driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]"));
//    ActionMethods.WaitByvisibilityOfElement(seek_bar, 5);
//	ActionMethods.tapOnElement(seek_bar);
	test.log(Status.PASS, "Sleep duration is added");
	MobileElement setSleep =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton"));
	 
	setSleep.click();
	test.log(Status.PASS, "Save button is clicked  and  sleep score updated");
//	String sleeppc=driver.findElement(By.id("fittnes_score")).getText();
//	System.out.println(sleeppc);
	Thread.sleep(3000);
}

@Test(priority=4,enabled=true)
void addcalmnessdata() throws InterruptedException
{
	test = extent.createTest("ADD CALMNESS DATA","This test case will add sleep data");
	test.log(Status.INFO, "Test Case Started");
	MobileElement e1= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]"));
	ActionMethods.tapOnElement(e1);
	test.log(Status.PASS, "Clamness Add "+" button is clicked");
//	MobileElement e2= driver.findElement(By.xpath(""));
//	ActionMethods.tapOnElement(e2);
//	test.log(Status.PASS, "Calmness value is checked ");
	
	//XCUIElementTypeButton[@name="54CB16DE A778 480E 99A2 77B12B"]
	MobileElement setcalmness =driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton"));
	ActionMethods.WaitByvisibilityOfElement(setcalmness, 5);
    ActionMethods.tapByElement(setcalmness);
	test.log(Status.PASS, "Save button is clicked and calmness score updated");
	test.log(Status.PASS, "Mind Score is updated ");
}
public void AcceptAlert()

{
	  test =extent.createTest("Allow Application Alert","");
		test.log(Status.INFO,"Test case to Allow Application to Install");
	  HashMap<String, String> param = new HashMap<String, String>();
		param.put("action", "getButtons");
		List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);		
		for(String button: buttons) {			
			System.out.println(button);
			if(button.equals("Yes")|| button.equals("yes")) {
				//Accept for No and dismiss for Yes
				param.put("action", "accept");
				driver.executeScript("mobile: alert", param);
				break;
			}
		}
			test.log(Status.PASS,"Alert has been allowed");
}

@Test(priority=5 , enabled=true)
void deletemindscore() throws InterruptedException
{
	test = extent.createTest("DELETE MIND SCORE","This test case will click on delete button");
	test.log(Status.INFO, "Test Case Started");
	MobileElement delete=driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]"));
	ActionMethods.WaitByvisibilityOfElement(delete, 5);
	delete.click();
	test.log(Status.PASS, "delete button is clicked");	
}
@Test(priority=6 , enabled=true)
void deletSleepcore() throws InterruptedException
{
	test = extent.createTest("DELETE SLEEP SCORE","This test case will delete sleep score individually");
	test.log(Status.INFO, "Started");
   MobileElement deleteicon=driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton[2]"));
   ActionMethods.WaitByvisibilityOfElement(deleteicon, 5);
   ActionMethods.tapByElement(deleteicon);
   
	//driver.findElement(By.xpath("")).click();
   

   driver.switchTo().alert().dismiss();
	test.log(Status.PASS, "Sleep score is deleted");	
	Thread.sleep(3000);
}

@Test(priority=7 , enabled=true)
void deletCalmnessscore() throws InterruptedException
{
	deletemindscore();
	test = extent.createTest("DELETE CALMNESS SCORE","This test case will delete calmness score individually");
	test.log(Status.INFO, "Started");
	MobileElement deleteCalmness = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton[3]"));
	deleteCalmness.click();
	driver.switchTo().alert().dismiss();
	test.log(Status.PASS, "Calmness score is deleted");	
	
	Thread.sleep(3000);

}
@Test(priority=8 , enabled=false)
void resetall() throws InterruptedException
{
	test = extent.createTest("RESET MIND SCORE","This test case will reset mind score");
	test.log(Status.INFO, "Test Case Started");
	driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"RESET ALL\"]")).click();
	
	//XCUIElementTypeButton[@name="CANCEL"]
	test.log(Status.PASS, "Reset All button is clicked ");	
	Thread.sleep(3000);
	//driver.findElement(By.id("btn_done")).click();
	test.log(Status.PASS, "Mind is score deleted ");	
	Thread.sleep(3000);
	
}
@Test(priority=9, enabled=true)
public void BackToHealthScore() {
 MobileElement backButton= driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"MIND\"]/XCUIElementTypeButton[1]"));
 backButton.click();
 
	
}
}