package test;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class Login extends BaseClass {
	public ExtentTest test;
	//public static String MobileNumber="500666778";
	public static String MobileNumber="577887788";
	public static String otp_digit_0="0";
	public static String otp_digit_1="1";
	public static String two="2";
			
	  @Test (priority =0, enabled=true)
	  public void AcceptAlert()
	  
	  {
		  test =extent.createTest("Allow Application Alert","");
			test.log(Status.INFO,"Test case to Allow Application to Install");
		  HashMap<String, String> param = new HashMap<String, String>();
			param.put("action", "getButtons");
			List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);		
			for(String button: buttons) {			
				//System.out.println(button);
				if(button.equals("Allow")) {
					param.put("action", "accept");
					driver.executeScript("mobile: alert", param);
					break;
				}
			}
				test.log(Status.PASS,"Alert has been allowed");
	  }
	  
	  @Test (priority =1, enabled=true)
public void SelectLanguage() {
		  test =extent.createTest("Select App language","Test Case will select application language");
			test.log(Status.INFO,"Test case Started to Select Language");
		// MobileElement languageAr = driver.findElement(MobileBy.AccessibilityId("ar app lang unselec"));
		// languageAr.click();
		//	test.log(Status.PASS,"Clicked on arabic");
		 MobileElement languageSelection = driver.findElement(MobileBy.AccessibilityId("continue"));
		  languageSelection.click();
		  test.log(Status.PASS,"Clicked on Continue Button on language Screen");
       	  }
	  
	  @Test (priority=2, enabled=true)
	  public void EnterMobileNumber() {
		  test =extent.createTest("Enter Mobile Number","Test Case for Entering mobile number");
			test.log(Status.INFO,"Test case for Entering mobile number");
		 MobileElement inputNumber = driver.findElement(MobileBy.AccessibilityId("Enter Mobile Number"));
	         //WebElement inputNumber = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/"+
	          //       "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	     ActionMethods.WaitByvisibilityOfElement(inputNumber, 5000);            
		 inputNumber.sendKeys(MobileNumber);
	                 test.log(Status.PASS,"Mobile number Entered");
	                // WebElement EnterMobileNumber = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]"));
	                // EnterMobileNumber.click();
	                 MobileElement languageSelection =  driver.findElement(MobileBy.AccessibilityId("continue"));
	       		  languageSelection.click();
	       		test.log(Status.PASS,"Clicked on Continue button on mobile number Screen");
	  }
	  @Test (priority =3, enabled=true)
	  public void EnterOTP(){
		  test =extent.createTest("Enter OTP","Test Case for Entering OTP");
			test.log(Status.INFO,"Test case Started for entering OTP");
		 String pin1xpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
			  "XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
		// 	//XCUIElementTypeTextField[@name=\"PinField\"][1]
		// String pin1xpath="//XCUIElementTypeTextField[@name=\"PinField\"][1]";	  
		// wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin1xpath)));
		MobileElement otp1 = driver.findElement(By.xpath(pin1xpath));
		ActionMethods.WaitByvisibilityOfElement(otp1, 5);
		  otp1.sendKeys("0");
		  test.log(Status.PASS,"Zero entered");
		  String pin2xpath= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
				  "XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	  //String pin2xpath="//XCUIElementTypeTextField[@name=\"PinField\"][2]";
	         MobileElement otp2 = driver.findElement(By.xpath(pin2xpath));
	         ActionMethods.WaitByvisibilityOfElement(otp2, 5);
	         otp2.sendKeys("1");
	         test.log(Status.PASS,"one entered");
//	         for (int i=1; i>=6;i++) {
//	        	 String pinxpath= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
//	   				  "XCUIElementTypeCollectionView/XCUIElementTypeCell["+i+"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
//	   		 
//	        	 wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pinxpath)));
//	   	         WebElement otp = driver.findElement(By.xpath(pinxpath));
//	   	      if ( i % 2 == 0)
//	   	    	  otp.sendKeys("0");
//	   	      else
//	   	    	  otp.sendKeys("1");
//	         }
//	        // if ( n % 2 == 0)
//	          //   cout << n << " is even.";
//	         //else
//	           //  cout << n << " is odd.";
	         String pin3xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	   	      	  "XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	         //String pin3xpath="//XCUIElementTypeTextField[@name=\"PinField\"][3]";
	         MobileElement otp3 = driver.findElement(By.xpath(pin3xpath));
	         ActionMethods.WaitByvisibilityOfElement(otp3, 5);
	         otp3.sendKeys("0");
	         test.log(Status.PASS,"Zero entered");
	        String pin4xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	    	       		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin4xpath)));
	         
	      //  String pin4xpath="//XCUIElementTypeTextField[@name=\"PinField\"][4]";
	         MobileElement otp4 = driver.findElement(By.xpath(pin4xpath));
	         ActionMethods.WaitByvisibilityOfElement(otp4, 5);
	         otp4.sendKeys("1");
	         test.log(Status.PASS,"One entered");
	         String pin5xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	     	    	 "XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin5xpath)));
	         //String pin5xpath="//XCUIElementTypeTextField[@name=\"PinField\"][5]";
	         MobileElement otp5 = driver.findElement(By.xpath(pin5xpath));
	         ActionMethods.WaitByvisibilityOfElement(otp5, 5);
	         otp5.sendKeys("0");
	         test.log(Status.PASS,"Zero entered");
	         String pin6xpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
		       		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(pin6xpath)));
	         //WebElement otp6 = driver.findElement(By.xpath(pin6xpath));
	         
	       //String pin6xpath="//XCUIElementTypeTextField[@name=\"PinField\"][6]";
	         MobileElement otp6 = driver.findElement(By.xpath(pin6xpath));
	         ActionMethods.WaitByvisibilityOfElement(otp6, 5);
	         otp6.sendKeys("1");
	         test.log(Status.PASS,"One entered");
	         test.log(Status.PASS,"OTP has been entered successfully");
    }
	  @Test (priority =4,enabled=true)
public void EnterPin() throws InterruptedException {
	  // Enter PIN
		  test =extent.createTest("Enter Pin","Test Case for Entering PIN");
			test.log(Status.INFO,"Test case Started for entering PIN");
		 	  
    MobileElement pin1 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
			"XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	//		 String pin1xpath="//XCUIElementTypeStaticText[@name=\"pincell\"][1]";
	  //       MobileElement pin1 = driver.findElement(By.xpath(pin1xpath));
	         ActionMethods.WaitByvisibilityOfElement(pin1,5);
			 pin1.sendKeys("1");
       test.log(Status.PASS,"Pin digit entered");
       MobileElement pin2 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+     			
       "XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
       //String pin2xpath="//XCUIElementTypeStaticText[@name=\"pincell\"][2]";
       //MobileElement pin2 = driver.findElement(By.xpath(pin2xpath));
       ActionMethods.WaitByvisibilityOfElement(pin2,5);
       pin2.sendKeys("1");
       test.log(Status.PASS,"Pin digit entered");
       MobileElement pin3 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+  
      "XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
      // String pin3xpath="//XCUIElementTypeStaticText[@name=\"pincell\"][3]";
      // MobileElement pin3 = driver.findElement(By.xpath(pin3xpath));
       //ActionMethods.WaitByvisibilityOfElement(pin3,5);
       pin3.sendKeys("1");
       test.log(Status.PASS,"Pin digit entered");
       MobileElement pin4 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
   			"XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
       //String pin4xpath="//XCUIElementTypeStaticText[@name=\"pincell\"][4]";
       //MobileElement pin4 = driver.findElement(By.xpath(pin4xpath));
      // ActionMethods.WaitByvisibilityOfElement(pin4,5);
       pin4.sendKeys("1");
       test.log(Status.PASS,"Pin digit entered");
       test.log(Status.PASS,"Pin entered Successfully");
       Thread.sleep(2000);
}
	  @Test (priority =5,enabled =true)
public void HomeScreen() {
		 
		//  WebElement medicalProfile = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"MEDICAL PROFILE\"]"));
//	 medicalProfile.click();
	 try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		  
	  }

//@Test(priority=6,enabled=true)
//public void clickOnMenuIcon(){
//	test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
//	test.log(Status.INFO,"Test case Started");	
//
//	driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
//	test.log(Status.INFO, "clicked Menu Icon on Home Screen");
//}
//@Test(priority=7,enabled=true)
//public void tapOnMedicalprofile(){
//
//	test =extent.createTest("CLICK MEDICAL PROFILE","This Test Case will click on medical profile.");
//	test.log(Status.INFO,"Test case Started");
//	driver.findElement(MobileBy.AccessibilityId("medical profile")).click();
//	test.log(Status.PASS, "Medical profile clicked");
//	try {
//		Thread.sleep(2000);
//	} catch (InterruptedException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//}
//
//@Test(priority=8, enabled=true)
//public void personalProfileSetHeight() throws InterruptedException{	
//
//	test =extent.createTest("PERSONAL PROFILE SET HEIGHT","This Test Case will set the height of patient.");
//	test.log(Status.INFO,"Test case Started");
//	
//	 MobileElement height =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"height\"]");
////	 ActionMethods.WaitByvisibilityOfElement(height, 2000);
//	 ActionMethods.tapOnElement(height);
//	//driver.findElement(MobileBy.AccessibilityId("height")).click();
//	test.log(Status.PASS, "Clicked on Height");
//	//driver.findElement(MobileBy.AccessibilityId("height")).click();
//	
//	MobileElement picker =driver.findElementByClassName("XCUIElementTypePickerWheel");
//	ActionMethods.WaitByvisibilityOfElement(picker, 2000);
//	picker.sendKeys("55");
//	//XCUIElementTypePickerWheel
//	//MobileElement addheight = driver.findElement(By.id("wheel_height"));
//	//ActionMethods.ScrollDown(, y_start, x_stop, y_stop, duration);
//	//ActionMethods.verticallyUp(addheight, 200);
//	
//	test.log(Status.PASS, "Select value from list");
//	MobileElement savebtn =driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Save\"]"));
//	ActionMethods.tapOnElement(savebtn);
//	test.log(Status.PASS, "Clicked on save button");
//}
//
//@Test(priority=9,enabled=true)
//public void verifyCancelButtonSetHeight(){	
//
//	test =extent.createTest("SET HEIGHT CANCEL BUTTON","This Test Case will verify cancel Button is Functional.");
//	 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS) ;
//	MobileElement height =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"height\"]");
//	
//	// ActionMethods.WaitByvisibilityOfElement(height, 2000);
//height.click();
//
//test.log(Status.PASS, "Clicked on height");
//
//	driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Cancel\"]")).click();
//	test.log(Status.PASS, "Clicked on Cancel Button");
//}

}