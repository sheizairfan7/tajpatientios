package test;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.sun.org.apache.xpath.internal.operations.Bool;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class FamilyMember extends BaseClass {
	 public ExtentTest test;
	  Point p;
		WebDriverWait wait;
		 @Test(priority=0,enabled=true)
		    public void clickOnMenuIcon() throws InterruptedException{
		        test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		        test.log(Status.INFO,"Test case Started");    
		    	driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
		    	test.log(Status.INFO, "clicked Menu Icon on Home Screen");
		       
		    }
		    
		 
		    @Test(priority=1 , enabled=true)
		    public void clickonfamilymember() throws InterruptedException 
		    {
		        test = extent.createTest("ClICK ON FAMILY MEMBERS","This test case will click on family members from menu");
		        test.log(Status.INFO, "Test case Started");
		     String familyMemberxpath= "//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]";
		       // MobileElement familyMembericon = driver.findElement(MobileBy.AccessibilityId("Family Members"));
		        MobileElement familyMembericon =driver.findElement(By.xpath(familyMemberxpath));
		        ActionMethods.tapByElement(familyMembericon);
		        //familyMembericon.click();
		        test.log(Status.PASS, "Family member button clicked");
		        Thread.sleep(3000);
		    }
		    @Test(priority=2,enabled=true)
		    public void AddRelativeAssertion() throws InterruptedException{
	Boolean  assertionResult=true;
		    	SoftAssert assertion= new SoftAssert();
		    	try {
		    	MobileElement Add_Relative= driver.findElement(By.xpath(
		    			"//XCUIElementTypeStaticText[@name=\"Add Family\"]"));
		    	ActionMethods.WaitByvisibilityOfElement(Add_Relative, 5);
		        assertionResult=	Add_Relative.isDisplayed();
		        //System.out.println("Assertion for Add Family member"+ Add_Relative.isDisplayed() );
		        	assertion.assertTrue(Add_Relative.isDisplayed());
		    	}
//if(assertionResult==true) {
		    	catch(Exception e){
		    		throw new SkipException("Skip this testCase");
		    		
		    	}
		    }
		    @Test(priority=3, enabled=true, dependsOnMethods="AddRelativeAssertion")
		    public void addFirstMember() {
		    	
		    	MobileElement fathericon= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]"));
	            ActionMethods.WaitByvisibilityOfElement(fathericon, 5000);
	            ActionMethods.tapByElement(fathericon);
	            test.log(Status.PASS, "Father icon is clicked");
	        	
	         String InsuranceIDxpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
	         MobileElement insuranceID = driver.findElement(By.xpath(InsuranceIDxpath));
	         ActionMethods.WaitByvisibilityOfElement(insuranceID, 5000);   
	         insuranceID.sendKeys("2171615095");
	       //  test.log(Status.PASS, "Father insurance ID entered");
	       MobileElement tag = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"National ID\"]"));
	       ActionMethods.WaitByvisibilityOfElement(tag, 5000);
	       ActionMethods.tapOnElement(tag);
	            test.log(Status.PASS, "national id is entered");
	            //XCUIElementTypeStaticText[@name="Mobile No."]
	            String MobileNumber ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
	       MobileElement number=  driver.findElement(By.xpath(MobileNumber));
	          ActionMethods.WaitByvisibilityOfElement(number, 5000);
	          number.sendKeys("558989666"); 
	          test.log(Status.PASS, "mobile number is entered");
	          MobileElement numberText=  driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Mobile No.\"]"));
	          ActionMethods.WaitByvisibilityOfElement(number, 2000);
	          ActionMethods.tapByElement(numberText);
	            driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"SAVE\"]")).click();
	            test.log(Status.PASS, "save button is clicked");
		    }
	 //   @Test(priority=3 ,enabled=true)
	    public void addrelative() throws InterruptedException 
	    {
	        test= extent.createTest("ADD FAMILY MEMBER","This test case will add family member");
	        test.log(Status.INFO, "Test case Started");
	      //  MobileElement addButton =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"addMembers1\"]");
	        MobileElement addButton = driver.findElement(MobileBy.AccessibilityId("addMembers1"));
	   	   ActionMethods.WaitByvisibilityOfElement(addButton , 5000);
	   	   addButton.click();
	   	test.log(Status.PASS, "Add button is clicked");
	   	  // ActionMethods.tapByElement(addButton);
	      //XCUIElementTypeButton[@name="addMembers1"]
	   	   
	        String  title=driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Add Family\"]")).getAttribute("value");
	        ////XCUIElementTypeNavigationBar[@name=\"Add Family\"]
	        SoftAssert softassert = new SoftAssert();
	        softassert.assertEquals(title, title.equals("Add Family")||title.equals("ADD FAMILY"));
	        try
	        {
	            test = extent.createTest("Test Case to add Relative");
	            test.log(Status.INFO, "Test case Started");
	          //XCUIElementTypeStaticText[@name="Father"]
	            //MobileElement fathericon=driver.findElement(By.id("iv_father"));
	           MobileElement fathericon= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]"));
	            ActionMethods.WaitByvisibilityOfElement(fathericon, 5000);
	            ActionMethods.tapByElement(fathericon);
	            test.log(Status.PASS, "Father icon is clicked");
	        	
	         String InsuranceIDxpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
	         MobileElement insuranceID = driver.findElement(By.xpath(InsuranceIDxpath));
	         ActionMethods.WaitByvisibilityOfElement(insuranceID, 5000);   
	         insuranceID.sendKeys("2171615095");
	       //  test.log(Status.PASS, "Father insurance ID entered");
	       MobileElement tag = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"National ID\"]"));
	       ActionMethods.WaitByvisibilityOfElement(tag, 5000);
	       ActionMethods.tapOnElement(tag);
	            test.log(Status.PASS, "national id is entered");
	            //XCUIElementTypeStaticText[@name="Mobile No."]
	            String MobileNumber ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
	       MobileElement number=  driver.findElement(By.xpath(MobileNumber));
	          ActionMethods.WaitByvisibilityOfElement(number, 5000);
	          number.sendKeys("558989857"); 
	          test.log(Status.PASS, "mobile number is entered");
	          MobileElement numberText=  driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Mobile No.\"]"));
	          ActionMethods.WaitByvisibilityOfElement(number, 2000);
	          ActionMethods.tapByElement(numberText);
	            driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"SAVE\"]")).click();
	            test.log(Status.PASS, "save button is clicked");
	           // Thread.sleep(3000);
	           // VerifyDetails();
	        }
	        catch(Exception message)
	        {
	            test.log(Status.FAIL,"test failed" );
	        }
	    }
	    @Test(priority=4 ,enabled=true,dependsOnMethods="AddRelativeAssertion")
	    public void VerifyDetails() throws InterruptedException
	    {
	        
//	        SoftAssert softassert = new SoftAssert();
//	        softassert.assertTrue(SaveBtn.isDisplayed());
	    	 
	    	test= extent.createTest("VERIFY DETAILS","This test case will verify details");
	            test.log(Status.INFO, "Test case Started");
	            Thread.sleep(3000);  
	            MobileElement SaveBtn=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Save\"]"));
		        ActionMethods.WaitByvisibilityOfElement(SaveBtn, 2000);
	            SaveBtn.click();
		        //ActionMethods.tapByElement(SaveBtn);
	            test.log(Status.PASS, "Family member details verified");
	            Thread.sleep(5000);
	        	test.log(Status.PASS, "Family Member added Successfully");	       
	        	
		        	
	  
	    }
	    @Test(priority=5, enabled=true,dependsOnMethods="AddRelativeAssertion")
public void FamilyMemberVerification() {
	    	try {
        		SoftAssert assertion= new SoftAssert();
        	MobileElement tab =driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Family Members\"]"));
        	ActionMethods.WaitByvisibilityOfElement(tab, 5);
	      Boolean   assertionResult=tab.isDisplayed();
	        //System.out.println("Assertion for Add Family member"+ Add_Relative.isDisplayed() );
	        	assertion.assertTrue(tab.isDisplayed());
	        	if(assertionResult==true) {
	        		MobileElement homebtn= driver.findElement(By.xpath(
	        				"//XCUIElementTypeNavigationBar[@name=\"AER_QA.PatientProfileView\"]/XCUIElementTypeButton[2]"));
	        		homebtn.click();
	        	}	}catch(Exception e) {
	        		throw new SkipException("Skip testcase in the case Assertion fasle");
	        	}
	    }
	    @Test(priority=5, enabled= false)
	    public void AddSonOrDaughter() {
	    	
	    	 test= extent.createTest("ADD FAMILY MEMBER","This test case will add family member");
		        test.log(Status.INFO, "Test case Started");
		      //  MobileElement addButton =driver.findElementByXPath("//XCUIElementTypeButton[@name=\"addMembers1\"]");
		        MobileElement addButton = driver.findElement(MobileBy.AccessibilityId("addMembers1"));
		   	   ActionMethods.WaitByvisibilityOfElement(addButton , 5000);
		   	   addButton.click();
		   	test.log(Status.PASS, "Add button is clicked");
		   	  // ActionMethods.tapByElement(addButton);
		      //XCUIElementTypeButton[@name="addMembers1"]
		   	   
		        String  title=driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Add Family\"]")).getAttribute("value");
		        ////XCUIElementTypeNavigationBar[@name=\"Add Family\"]
		        SoftAssert softassert = new SoftAssert();
		        softassert.assertEquals(title, title.equals("Add Family")||title.equals("ADD FAMILY"));
		        try
		        {
		            test = extent.createTest("Test Case to add Relative");
		            test.log(Status.INFO, "Test case Started");
		          //XCUIElementTypeStaticText[@name="Father"]
		            //MobileElement fathericon=driver.findElement(By.id("iv_father"));
		           MobileElement sonIcon= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]"));
		            ActionMethods.WaitByvisibilityOfElement(sonIcon, 5000);
		            ActionMethods.tapByElement(sonIcon);
		            test.log(Status.PASS, "Son icon is clicked");
		            Thread.sleep(3000);
		        	//Handle Alert
		            driver.switchTo().alert().accept();
		            Thread.sleep(3000);
		            //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		         String InsuranceIDxpath ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[1]";
		         MobileElement insuranceID = driver.findElement(By.xpath(InsuranceIDxpath));
		         ActionMethods.WaitByvisibilityOfElement(insuranceID, 5000);   
		         insuranceID.sendKeys("2171615095");
		       //  test.log(Status.PASS, "Father insurance ID entered");
		       MobileElement tag = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"National ID\"]"));
		       ActionMethods.WaitByvisibilityOfElement(tag, 5000);
		       ActionMethods.tapOnElement(tag);
		            test.log(Status.PASS, "national id is entered");
		            //XCUIElementTypeStaticText[@name="Mobile No."]
		            String MobileNumber ="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField[2]";
		          MobileElement number=  driver.findElement(By.xpath(MobileNumber));
		          ActionMethods.WaitByvisibilityOfElement(number, 5000);
		          number.sendKeys("558989857"); 
		          test.log(Status.PASS, "mobile number is entered");
		          MobileElement numberText=  driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Mobile No.\"]"));
		          ActionMethods.WaitByvisibilityOfElement(number, 2000);
		          ActionMethods.tapByElement(numberText);
		          MobileElement savebtn =  driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"SAVE\"]"));
		          ActionMethods.tapByElement(savebtn);	            
		            test.log(Status.PASS, "save button is clicked");
		           Thread.sleep(3000);
		          //  VerifyDetails();
		        }
		        catch(Exception message)
		        {
		            test.log(Status.FAIL,"test failed" );
		        }
	    }
	}