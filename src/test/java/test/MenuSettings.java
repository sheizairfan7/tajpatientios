package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class MenuSettings extends BaseClass {
	String pin="1";
	String Newpin="2";
	@Test(priority=1,enabled=false)
	public void clickOnMenuIcon(){
		test =extent.createTest("CLICK MENU ICON","This Test case will click on Menu Icon to select medical profile From Home Screen.");
		test.log(Status.INFO,"Test case Started");	

		driver.findElement(MobileBy.AccessibilityId("icMenuWhite")).click();	
		test.log(Status.INFO, "clicked Menu Icon on Home Screen");
	}
	
	@Test(priority=3 ,enabled=true)
	public void Settings() {
		MobileElement settings=driver.findElement(MobileBy.AccessibilityId("menu_settings-2"));
		//MobileElement settings= driver.findElement(By.xpath("//XCUIElementTypeImage[@name=\"menu_settings-2\"]"));
		ActionMethods.tapByElement(settings);
	}
@Test(priority=4, enabled=false)
public void doctorPreference() throws InterruptedException {
	
	MobileElement doctorPreference= driver.findElement(By.xpath(
			"//XCUIElementTypeStaticText[@name=\"Doctor Gender Preference\"]"));
	ActionMethods.tapByElement(doctorPreference);
	//XCUIElementTypeStaticText[@name="Select Doctor Gender"]
	Thread.sleep(3000);
	MobileElement element= driver.findElement(By.xpath(
			"//XCUIElementTypeImage[@name=\"male\"]"));
	ActionMethods.tapByElement(element);
	MobileElement continueBtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"));
	ActionMethods.tapByElement(continueBtn);
	
//	MobileElement backbtn= driver.findElement(By.xpath( 
//			"//XCUIElementTypeNavigationBar[@name=\"AER_QA.DocGenderSelectionView\"]/XCUIElementTypeButton[1]"));
//	ActionMethods.WaitByvisibilityOfElement(backbtn, 5);
//	ActionMethods.tapByElement(backbtn);
//	Thread.sleep(3000);
}
@Test(priority=5 ,enabled=false)
public void doctorLanguage() throws InterruptedException {
	//XCUIElementTypeStaticText[@name="Choose The Doctor Language"]
		MobileElement doctorLang= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Choose Doctor Language\"]"));
	    ActionMethods.WaitByvisibilityOfElement(doctorLang, 5);
		ActionMethods.tapByElement(doctorLang);
	    
		//Thread.sleep(2000);
		MobileElement lang= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]"));
		ActionMethods.WaitByvisibilityOfElement(lang, 5);
		Thread.sleep(2000);
		ActionMethods.tapByElement(lang);
		MobileElement continueBtn1= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Continue\"]"));
		ActionMethods.tapByElement(continueBtn1);
		
//		MobileElement backbtn1= driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"AER_QA.SpokenLanguageView\"]/XCUIElementTypeButton[1]"));
//		ActionMethods.WaitByvisibilityOfElement(backbtn1, 5);
//		ActionMethods.tapByElement(backbtn1);
}
	//profile 
	
	//XCUIElementTypeStaticText[@name="Profile"]
@Test(priority=6 ,enabled=false)
	public void profile() throws InterruptedException {
	MobileElement profile= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Profile\"]"));
    ActionMethods.tapByElement(profile);
	Thread.sleep(3000);
	}
@Test(priority=7 ,enabled=false)
	public void logout() throws InterruptedException {
	
	MobileElement logout= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Logout\"]"));
    ActionMethods.tapByElement(logout);
    Thread.sleep(3000);
    //driver.switchTo().alert().dismiss();
//    @Test (priority =0, enabled=true)
//	  public void CancelAlert()
//	  
//	  {
}
@Test(enabled=false)
public void logoutDialouge() {
		  test =extent.createTest("Allow Application Alert","");
			test.log(Status.INFO,"Test case to Allow Application to Install");
//		  HashMap<String, String> param = new HashMap<String, String>();
//			param.put("action", "getButtons");
//			List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);		
//			for(String button: buttons) {			
//				System.out.println(button);
//				if(button.equals("Cancel")) {
//					param.put("action", "dismiss");
//					driver.executeScript("mobile: alert", param);
//					break;
//				}
//			}
			MobileElement alert= driver.findElement(By.xpath("//XCUIElementTypeAlert[@name=\"All Local Data will be Deleted\"]"));
		    ActionMethods.WaitByvisibilityOfElement(alert, 5);
			MobileElement cancleButton= driver.findElement(By.xpath("//XCUIElementTypeAlert[@name=\"All Local Data will be Deleted\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]"));
			ActionMethods.tapByElement(alert);
			
			ActionMethods.tapByElement(cancleButton);
//			MobileElement acceptButton= driver.findElement(By.xpath("//XCUIElementTypeAlert[@name=\"All Local Data will be Deleted\"]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]"));	
//			ActionMethods.tapByElement(acceptButton);
			test.log(Status.PASS,"Alert has been cancel");
				
				MobileElement backbtn1= driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"Profile\"]/XCUIElementTypeButton[1]"));
				ActionMethods.WaitByvisibilityOfElement(backbtn1, 5);
				ActionMethods.tapByElement(backbtn1);		
			//	Thread.sleep(5000);
}

//}

@Test(priority=7, enabled=true)
public void changePin() {
 MobileElement changePin= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"Change PIN\"]"));
 ActionMethods.tapByElement(changePin);
//MobileElement pinCell1= driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name=\"pincell\"][1]"));
//pinCell1.sendKeys("1");

}
@Test(priority=8, enabled=true)
public void EnterOldPin() throws InterruptedException {
	test =extent.createTest("Enter Pin","Test Case for Entering PIN");
	test.log(Status.INFO,"Test case Started for entering PIN");
	 String pin1xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
     MobileElement pin1 = driver.findElement(By.xpath(pin1xpath));
     ActionMethods.WaitByvisibilityOfElement(pin1,5);
	 pin1.sendKeys(pin);
test.log(Status.PASS,"Pin digit entered");

String pin2xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
MobileElement pin2 = driver.findElement(By.xpath(pin2xpath));
ActionMethods.WaitByvisibilityOfElement(pin2,5);
pin2.sendKeys(pin);
test.log(Status.PASS,"Pin digit entered");  
String pin3xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
MobileElement pin3 = driver.findElement(By.xpath(pin3xpath));
//ActionMethods.WaitByvisibilityOfElement(pin3,5);
pin3.sendKeys(pin);
test.log(Status.PASS,"Pin digit entered");

String pin4xpath="//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
MobileElement pin4 = driver.findElement(By.xpath(pin4xpath));
// ActionMethods.WaitByvisibilityOfElement(pin4,5);
pin4.sendKeys(pin);
test.log(Status.PASS,"Pin digit entered");
//test.log(Status.PASS,"Pin entered Successfully");
//XCUIElementTypeStaticText[@name="Current PIN"]
MobileElement currentPin= driver.findElement(MobileBy.AccessibilityId("Current PIN"));
ActionMethods.tapByElement(currentPin);

}
@Test(priority=9, enabled=true)
public void EnterNewPin() {
	String pin5xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin5 = driver.findElement(By.xpath(pin5xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin5,5);
	pin5.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	String pin6xpath= 
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin6 = driver.findElement(By.xpath(pin6xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin6,5);
	pin6.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");

	String pin7xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin7 = driver.findElement(By.xpath(pin7xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin7,5);
	pin7.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	String pin8xpath= 
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin8 = driver.findElement(By.xpath(pin8xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin8,5);
	pin8.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	MobileElement newPin= driver.findElement(MobileBy.AccessibilityId("New PIN"));
	ActionMethods.tapByElement(newPin);
	
}
@Test(priority=10,enabled=true)
public void EnterPinAgain() throws InterruptedException {
	String pin9xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin9 = driver.findElement(By.xpath(pin9xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin9,5);
	pin9.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	String pin10xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin10 = driver.findElement(By.xpath(pin10xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin10,5);
	pin10.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	String pin11xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin11 = driver.findElement(By.xpath(pin11xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin11,5);
	pin11.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	String pin12xpath=
			"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField";
	MobileElement pin12 = driver.findElement(By.xpath(pin12xpath));
	// ActionMethods.WaitByvisibilityOfElement(pin12,5);
	pin12.sendKeys(Newpin);
	test.log(Status.PASS,"Pin digit entered");
	MobileElement newPintag= driver.findElement(MobileBy.AccessibilityId("New PIN"));
	ActionMethods.tapByElement(newPintag);
	Thread.sleep(2000);
	

}
@Test(priority=11,enabled=false)
public void ClickContinueButton() {
	MobileElement continueBtn= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]"));
		ActionMethods.WaitByvisibilityOfElement(continueBtn, 3);
		ActionMethods.tapByElement(continueBtn);
		test.log(Status.PASS, "Pin changed Successfully");
		
}
@Test(priority=12, enabled=true)
public void ClickBackButton() {
	MobileElement backButton= driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"AER_QA.ChangePIN\"]/XCUIElementTypeButton[1]"));
	ActionMethods.WaitByvisibilityOfElement(backButton, 2);
	ActionMethods.tapByElement(backButton);
	test.log(Status.PASS,"Back button Clicked");

}
@Test(priority=13, enabled=true)
public void backtoMenu() {
	MobileElement backBtn= driver.findElement(By.xpath(
			"//XCUIElementTypeNavigationBar[@name=\"Settings\"]/XCUIElementTypeButton[2]"));
	ActionMethods.WaitByvisibilityOfElement(backBtn, 2);
	ActionMethods.tapByElement(backBtn);
}
}
