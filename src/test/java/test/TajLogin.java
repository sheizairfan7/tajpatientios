package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class TajLogin {
	
	

	 public static AppiumDriver<MobileElement> driver;
	  @BeforeTest
	  public void setup() throws Exception {
	    DesiredCapabilities capabilities = new DesiredCapabilities();
	    
	    //IPone 8 simulator
	    capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "iOS");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.2");
	    capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
	    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8");
	    capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
	    capabilities.setCapability("udid","862FE3B8-C3EB-4469-9D21-D3E5F505E874");
	    capabilities.setCapability(MobileCapabilityType.APP, "/Users/zubairaslam/Library/Developer/Xcode/DerivedData"+
	    		    "/AER_Health-gtkutsbfimyhuddqqqksqupdcuuh/Build/Products/Debug-iphonesimulator/AER QA.app");
	    //Simulator UDID
	    driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		   System.out.println("Driver initiated successfully");

}
	  
	  
	 

	@AfterTest
	  public void tearDown() throws Exception {
	   driver.quit();
	  }

	  @Test (priority =0)
	  public void AcceptAlert()
	  
	  {
		  //test =extent.createTest("Allow Application Alert","");
		//	test.log(Status.INFO,"Test case Started");
		  driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
		  HashMap<String, String> param = new HashMap<String, String>();
			param.put("action", "getButtons");
			List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);	
			for(String button: buttons) {
				
				//System.out.println(button);
				if(button.equals("Allow")) {
					param.put("action", "accept");
					driver.executeScript("mobile: alert", param);
					break;
					
				}
			}
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
	  }
	  
	  @Test (priority =1)
  public void SelectLanguage() {
		  
		  WebDriverWait wait = new WebDriverWait(driver,15);
		  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]")));
		  WebElement languageSelection = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]"));
         languageSelection.click();

         	  }
	  
	  @Test (priority=2)
	  public void EnterMobileNumber() {
	         WebElement inputNumber = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/"+
	                 "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	                 inputNumber.sendKeys("557766880");
	                 WebElement EnterMobileNumber = driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"CONTINUE\"]"));
	                 EnterMobileNumber.click();
		  
	  }
	  @Test (priority =3)
	  public void EnterOTP(){
		 WebDriverWait wait = new WebDriverWait(driver,5);
		  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));
		WebElement otp1 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")); 
		  otp1.sendKeys("0");
		  
		  wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
			  "XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));
		  
	         WebElement otp2 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/"+
					  "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	         otp2.sendKeys("1");
	         
	      wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	      	  "XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));	         
	         
	         WebElement otp3 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/"+
					  "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	         otp3.sendKeys("0");
	        
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	       	//	  "XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));
	         
	         WebElement otp4 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/"+
					  "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	         otp4.sendKeys("1");
	        // wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	       	//	  "XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));
	        
	         WebElement otp5 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/"+
					  "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[5]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	         otp5.sendKeys("0");
	         
	         wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
	       		  "XCUIElementTypeCollectionView/XCUIElementTypeCell[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")));
	         WebElement otp6 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/"+
					  "XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[6]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
	         otp6.sendKeys("1");
	      
      }
	  @Test (priority =4)
public void EnterPin() {
	  // Enter PIN
		  
		  WebDriverWait wait = new WebDriverWait(driver,10);		  
      WebElement pin1 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
  			"XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
         pin1.sendKeys("1");
         WebElement pin2 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+     			
         "XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
         pin2.sendKeys("1");
         WebElement pin3 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+  
         "XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
         pin3.sendKeys("1");
         WebElement pin4 = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/"+
     			"XCUIElementTypeOther/XCUIElementTypeCollectionView/XCUIElementTypeCell[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField"));
         pin4.sendKeys("1");
 
	
}
	  @Test (priority =5)
public void HomeScreen() {
		  try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
	  }
}