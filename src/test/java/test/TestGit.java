package test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TestGit {

    public static AppiumDriver<MobileElement> driver;
    @BeforeTest
    public void setup() throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        //IPone 8 simulator
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "14.2");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8");
        capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        capabilities.setCapability("udid","862FE3B8-C3EB-4469-9D21-D3E5F505E874");
        capabilities.setCapability(MobileCapabilityType.APP, "/Users/zubairaslam/Library/Developer/Xcode/DerivedData"+
                "/AER_Health-gtkutsbfimyhuddqqqksqupdcuuh/Build/Products/Debug-iphonesimulator/AER QA.app");
        //Simulator UDID
        driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        System.out.println("Driver initiated successfully");

    }




    @AfterTest
    public void tearDown() throws Exception {
        driver.quit();
    }

    @Test(priority =0)
    public void AcceptAlert()

    {
        //test =extent.createTest("Allow Application Alert","");
        //	test.log(Status.INFO,"Test case Started");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS) ;
        HashMap<String, String> param = new HashMap<String, String>();
        param.put("action", "getButtons");
        List<String> buttons = (List<String>) driver.executeScript("mobile: alert", param);
        for(String button: buttons) {

            //System.out.println(button);
            if(button.equals("Allow")) {
                param.put("action", "accept");
                driver.executeScript("mobile: alert", param);
                break;
            }
        }
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS) ;
    }

}
