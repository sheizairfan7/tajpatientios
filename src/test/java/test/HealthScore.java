package test;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import utilities.ActionMethods;

public class HealthScore extends BaseClass{
	@Test(priority=0, enabled=true)
	public void HomeScreen_HealthScore() throws InterruptedException {
		test = extent.createTest("ClICK ON LIFESTYLESCORE","This test case will click on lifestyle score");
		test.log(Status.INFO, "Test Case Started");
		MobileElement element= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]"));
	    //ActionMethods.tapByElement(element);
		ActionMethods.WaitByvisibilityOfElement(element, 5);
		element.click();
		test.log(Status.PASS, "Heathscore button is clicked");
	Thread.sleep(4000);
	}
	@Test(priority=1, enabled=true)
	void fitnessbuttonclick() throws InterruptedException
	{
		test = extent.createTest("ClICK ON FITNESS BUTTON","This test case will click on lifestyle score");
		test.log(Status.INFO, "Started");
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"hui main fitness\"]")).click();
		//XCUIElementTypeButton[@name="hui main nutrition"]
		
		//XCUIElementTypeButton[@name="hui main heart"]
		test.log(Status.PASS, "fitness box is clicked");

	}
	@Test(priority=3, enabled=true)
	void addfitnessscore() throws InterruptedException
	{
		test = extent.createTest("ADD FITNESS SCORE","This test case will add fitness score");
		test.log(Status.INFO, "Started");
		MobileElement e1= driver.findElement(By.xpath(
				"//XCUIElementTypeButton[@name=\"Button\"]"));
		ActionMethods.tapByElement(e1);
		test.log(Status.PASS, "Add "+" button is clicked");
		MobileElement activity=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Button\"][2]"));
		ActionMethods.WaitByvisibilityOfElement(activity, 5);
		ActionMethods.tapByElement(activity);

		
		test.log(Status.PASS, "Run button is clicked");
		MobileElement scale=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Button\"][6]"));
		ActionMethods.WaitByvisibilityOfElement(scale, 5);
		ActionMethods.tapByElement(scale);
		test.log(Status.PASS, "Hard button is clicked");
	}
	@Test(priority=4)
	void adddistance() throws InterruptedException
	{
		//MobileElement e2=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Distance\"]"));
		MobileElement distance= driver.findElement(MobileBy.AccessibilityId("Distance"));
		ActionMethods.WaitByvisibilityOfElement(distance, 5);
		distance.click();
		//ActionMethods.tapOnElement(distance);
		test.log(Status.PASS, "Add distance button is clicked");
		
		MobileElement addKilometer= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[1]/XCUIElementTypePickerWheel"));
        ActionMethods.WaitByvisibilityOfElement(addKilometer, 5);
		ActionMethods.scrollOneStepsDown(addKilometer);
        test.log(Status.PASS, "kilometres list is scrolled");
        MobileElement addmeter= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[2]/XCUIElementTypePickerWheel"));
        ActionMethods.scrollOneStepsDown(addmeter);
        test.log(Status.PASS, "meters list is scrolled");
        
        MobileElement doneButton= driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Done\"]"));
     //   ActionMethods.WaitByvisibilityOfElement(addmeter, 2);
        ActionMethods.tapByElement(doneButton);
        test.log(Status.PASS, "distance is added");
      
	}
	@Test(priority=5)
	public void addTime()
	{
		//MobileElement e2=driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"Time\"]"));
        MobileElement time=driver.findElement(MobileBy.AccessibilityId("Time"));
		ActionMethods.WaitByvisibilityOfElement(time, 8);
		//ActionMethods.tapOnElement(time);
		
		time.click();
		test.log(Status.PASS, "Add distance button is clicked");
		
		MobileElement addHours= driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[1]/XCUIElementTypePickerWheel"));
        ActionMethods.WaitByvisibilityOfElement(addHours, 5);
		ActionMethods.scrollOneStepsDown(addHours);
        test.log(Status.PASS, "hour list is scrolled");
        MobileElement addMin= driver.findElement(By.xpath(
        		"//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypePicker[2]/XCUIElementTypePickerWheel"));
        ActionMethods.scrollOneStepsDown(addMin);
        test.log(Status.PASS, "Minute list is scrolled");
        
        MobileElement doneButton= driver.findElement(By.xpath(
        		"//XCUIElementTypeButton[@name=\"Done\"]"));
     //   ActionMethods.WaitByvisibilityOfElement(addmeter, 2);
        ActionMethods.tapByElement(doneButton);
        test.log(Status.PASS, "time is added");
	
	}
	@Test(priority=6, enabled=true)
	public void saveFitnessScore() throws InterruptedException {
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"02A29B7F 8377 46C9 A7A7 6BE4CD\"]")).click();
		test.log(Status.PASS, "Fitness score added");
		Thread.sleep(3000);
		
	}
	@Test(priority=7 , enabled=true)
	void deletCalmnessscore() throws InterruptedException
	{
		//deletemindscore();
		test = extent.createTest("DELETE CALMNESS SCORE","This test case will delete calmness score individually");
		test.log(Status.INFO, "Started");
		MobileElement deleteCalmness = driver.findElement(By.xpath("//XCUIElementTypeApplication[@name=\"TAJ\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton[3]"));
		deleteCalmness.click();
		driver.switchTo().alert().dismiss();
		test.log(Status.PASS, "Calmness score is deleted");	
		
		Thread.sleep(3000);

	}
	@Test(priority=8 , enabled=false)
	void resetall() throws InterruptedException
	{
		test = extent.createTest("RESET MIND SCORE","This test case will reset mind score");
		test.log(Status.INFO, "Test Case Started");
		driver.findElement(By.xpath("//XCUIElementTypeButton[@name=\"RESET ALL\"]")).click();
		
		//XCUIElementTypeButton[@name="CANCEL"]
		test.log(Status.PASS, "Reset All button is clicked ");	
		Thread.sleep(3000);
		//driver.findElement(By.id("btn_done")).click();
		test.log(Status.PASS, "Mind is score deleted ");	
		Thread.sleep(3000);
		
	}
	public void BacktoHealthScore() {
		
		driver.findElement(By.xpath("//XCUIElementTypeNavigationBar[@name=\"FITNESS\"]/XCUIElementTypeButton[1]")).click();
		
	}
	
	
}
