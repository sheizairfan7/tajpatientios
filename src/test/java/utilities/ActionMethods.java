package utilities;

import java.time.Duration;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;
import test.BaseClass;

public class ActionMethods extends BaseClass{
	public static Point point;
	public static void ScrollDown(int x_start, int y_start, int x_stop, int y_stop, int duration) {
		new TouchAction(driver).press(PointOption.point(x_start, y_start))
		.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
		.moveTo(PointOption.point(x_stop, y_stop)).release().perform();
	}
	public static void tapOnElement(MobileElement element)
	{
		point=element.getLocation();
		TouchAction<?> actions = new TouchAction(driver);
		actions.tap(PointOption.point(point.x, point.y)).perform();
		//System.out.println("This is child branch");
	}
	public static void WaitByvisibilityOfElement(MobileElement element,int WaitInSecond){
		WebDriverWait wait= new WebDriverWait(driver,WaitInSecond);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
public static void tapByElement(MobileElement element) {	
		new TouchAction(driver).tap(new TapOptions().withElement(ElementOption.element(element)))
		.waitAction(WaitOptions.waitOptions(Duration.ofMillis(250))).perform();
	}

public static void tapByCoordinates(int x, int y) {
	new TouchAction(driver).tap(PointOption.point(x, y))
	.waitAction(WaitOptions.waitOptions(Duration.ofMillis(1))).perform();
}

public static void scrollTwoStepsDown(MobileElement picker) {
	HashMap<String, Object> param = new HashMap<String, Object>();
	param.put("order", "next");
	param.put("offset", 0.30);
	param.put("element", picker);
	try {
	driver.executeScript("mobile: selectPickerWheelValue", param);
	} catch(Exception e) {

		System.out.println("Ignoring element exception, moving back one step");
		param.put("order", "previous");
		driver.executeScript("mobile: selectPickerWheelValue", param);
	}
}
public static void scrollOneStepsDown(MobileElement picker) {
	
	HashMap<String, Object> param = new HashMap<String, Object>();
	param.put("order", "next");
	param.put("offset", 0.15);
	param.put("element", picker);
	try {
		driver.executeScript("mobile: selectPickerWheelValue", param);
		
	} catch(Exception e) {
		System.out.println("Ignoring element exception, moving back one step");
		param.put("order", "previous");
		driver.executeScript("mobile: selectPickerWheelValue", param);
		
	}
}
public static void scrollTwoStepUp(MobileElement picker) {
	HashMap<String, Object> param = new HashMap<String, Object>();
	param.put("order", "previous");
	param.put("offset", 0.30);
	param.put("element", picker);
	//driver.executeScript("mobile: selectPickerWheelValue", param);
	try {
		driver.executeScript("mobile: selectPickerWheelValue", param);
		
	} catch(Exception e) {
		System.out.println("Ignoring element exception, moving back one step");
		param.put("order", "next");
		driver.executeScript("mobile: selectPickerWheelValue", param);
		
	}
}
public static void scrollOneStepUP(MobileElement picker) {
	HashMap<String, Object> param = new HashMap<String, Object>();
	param.put("order", "previous");
	param.put("offset", 0.15);
	param.put("element", picker);
	//driver.executeScript("mobile: selectPickerWheelValue", param);
	try {
		driver.executeScript("mobile: selectPickerWheelValue", param);
		
	} catch(Exception e) {
		System.out.println("Ignoring element exception, moving back one step");
		param.put("order", "next");
		driver.executeScript("mobile: selectPickerWheelValue", param);
	}
}
public static void scrollDownByExecuteMobile(RemoteWebElement element ) {
	//RemoteWebElement element = (RemoteWebElement)driver. findElement(By.xpath(tableView));
	String elementID = element.getId();
	HashMap<String, String> scrollObject = new HashMap<String, String>();
	scrollObject.put("element", elementID);
	scrollObject.put("direction", "down");
	driver.executeScript("mobile:scroll", scrollObject);
}
public static void scrollUpByExecuteMobile(RemoteWebElement element1) {
	String elementID1 = element1.getId();
	HashMap<String, String> scrollObjects = new HashMap<String, String>();
	scrollObjects.put("element", elementID1); // Only for ‘scroll in element’
	scrollObjects.put("direction", "up");
	driver.executeScript("mobile:scroll", scrollObjects);
}
public static void swipe(int x_start, int y_start, int x_stop, int y_stop, int duration) {

	new TouchAction(driver).press(PointOption.point(x_start, y_start))
			.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
			.moveTo(PointOption.point(x_stop, y_stop)).release().perform();
}


}
