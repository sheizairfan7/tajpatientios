package utilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Point;
import test.BaseClass;

import java.time.Duration;

public class UtilClass extends BaseClass {
    public static Point point;
    public static void ScrollDown(int x_start, int y_start, int x_stop, int y_stop, int duration) {
        new TouchAction(driver).press(PointOption.point(x_start, y_start))
                .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(duration)))
                .moveTo(PointOption.point(x_stop, y_stop)).release().perform();
    }
    public static void tapOnElement(MobileElement element)
    {
        point=element.getLocation();
        TouchAction<?> actions = new TouchAction(driver);
        actions.tap(PointOption.point(point.x, point.y)).perform();
        //System.out.println("This is child branch");
   // this change is made for practice purpose
// lets practice
    }
    
}
